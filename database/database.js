const sqlite3 = require('sqlite3').verbose(),
	path = require('path'),
	dbPath = path.resolve(__dirname, './UFilm.db');

let db = new sqlite3.Database(dbPath);

// db.run('DROP TABLE userMarks');
// db.run('DROP TABLE subscriptionTypes');
// db.run('DROP TABLE payments');
// db.run('DROP TABLE user_files');
// db.run('DROP TABLE resetToken');

db.run(`PRAGMA foreign_keys = ON`);

db.run(`CREATE TABLE IF NOT EXISTS users(
	\`id\` INTEGER  NOT NULL,
	\`login\` TEXT  NOT NULL,
	\`email\` TEXT  NOT NULL,
	\`password\` TEXT  NOT NULL,
	\`isAdmin\` TEXT  NOT NULL,
	PRIMARY KEY (\`id\`)
);`);

db.run(`CREATE TABLE IF NOT EXISTS tokens(
	\`id\` INTEGER  NOT NULL,
	\`userId\` INTEGER NOT NULL,
	\`token\` TEXT NOT NULL,
	\`refreshToken\` TEXT NOT NULL,
	PRIMARY KEY (\`id\`),
	FOREIGN KEY (userId) REFERENCES users (id)
);`);

db.run(`CREATE TABLE IF NOT EXISTS subscriptions(
	\`id\` INTEGER  NOT NULL,
	\`userId\` INTEGER NOT NULL,
	\`wasFreeUsed\` INTEGER NOT NULL,
	\`expires\` TEXT NOT NULL,
	PRIMARY KEY (\`id\`),
	FOREIGN KEY (userId) REFERENCES users (id)
);`);

db.run(`CREATE TABLE IF NOT EXISTS subscriptionTypes(
	\`id\` INTEGER  NOT NULL,
	\`duration\` INTEGER NOT NULL,
	\`price\` REAL NOT NULL,
	PRIMARY KEY (\`id\`)
);`);

db.run(`CREATE TABLE IF NOT EXISTS payments(
	\`id\` INTEGER  NOT NULL,
	\`userId\` INTEGER NOT NULL,
	\`typeId\` INTEGER NOT NULL,
	\`date\` TEXT NOT NULL,
	PRIMARY KEY (\`id\`),
	FOREIGN KEY (userId) REFERENCES users (id),
	FOREIGN KEY (typeId) REFERENCES subscriptionTypes (id)
);`);

db.run(`CREATE TABLE IF NOT EXISTS mediaContent(
	\`id\` INTEGER  NOT NULL,
	\`title\` TEXT NOT NULL,
	\`filePath\` TEXT NOT NULL,
	\`previewPath\` TEXT NOT NULL,
	\`description\` TEXT NOT NULL,
	\`genres\` TEXT NOT NULL,
	\`releaseYear\` INTEGER NOT NULL,
	PRIMARY KEY (\`id\`)
);`);

db.run(`CREATE TABLE IF NOT EXISTS userMarks(
	\`id\` INTEGER  NOT NULL,
	\`mediaId\` INTEGER NOT NULL,
	\`userId\` INTEGER NOT NULL,
	\`mark\` INTEGER NOT NULL,
	PRIMARY KEY (\`id\`),
	FOREIGN KEY (userId) REFERENCES users (id),
	FOREIGN KEY (mediaId) REFERENCES mediaContent (id) ON DELETE CASCADE
);`);

db.run(`CREATE TABLE IF NOT EXISTS genres(
	\`id\` INTEGER  NOT NULL,
	\`genre\` TEXT NOT NULL,
	PRIMARY KEY (\`id\`)
);`);

/*db.run(`CREATE TABLE IF NOT EXISTS files(
	\`id\` INTEGER  NOT NULL,
	\`filePath\` TEXT NOT NULL,
	\`fileName\` TEXT NOT NULL,
	\`extension\` TEXT NOT NULL,
	\`dateAdded\` TEXT NOT NULL,
	PRIMARY KEY (\`id\`)
);`);

db.run(`CREATE TABLE IF NOT EXISTS user_files(
	\`idUser\` INTEGER  NOT NULL,
	\`idFile\` INTEGER NOT NULL,
	\`owner\` TEXT NOT NULL,
	FOREIGN KEY (idUser) REFERENCES users (id),
	FOREIGN KEY (idFile) REFERENCES files (id)
);`);

db.run(`CREATE TABLE IF NOT EXISTS resetToken(
	\`idToken\` INTEGER  NOT NULL,
	\`idUser\` INTEGER  NOT NULL,
	\`token\` TEXT NOT NULL,
	\`expires\` TEXT NOT NULL,
	\`wasUsed\` TEXT NOT NULL,
	FOREIGN KEY (idUser) REFERENCES users (id),
	PRIMARY KEY (\`idToken\`)
);`);*/

module.exports = db;