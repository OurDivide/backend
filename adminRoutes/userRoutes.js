const { verifyAdminToken } = require("../middlewares/jwtMiddleware");
const getUsers = require('../adminModels/adminUsers').getUsers;

const express = require('express'),
	router = express.Router();

router.get('/getUsers', verifyAdminToken, (req, res) => {
	getUsers()
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

module.exports = router;