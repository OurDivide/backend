const { verifyAdminToken } = require("../middlewares/jwtMiddleware");
const getSubscriptions = require('../adminModels/adminSubscriptions').getSubscriptions;

const express = require('express'),
	router = express.Router();

router.get('/getSubscriptions', verifyAdminToken, (req, res) => {
	getSubscriptions()
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

module.exports = router;