const { verifyAdminToken, verifyAdminRefreshToken } = require("../middlewares/jwtMiddleware");
const { signInMiddleware } = require('../middlewares/authMiddleware');
const signIn = require('../models/auth').checkUserExistence;

const express = require('express'),
	router = express.Router();

router.post('/signIn', signInMiddleware, (req, res) => {
	const { login, password } = req.body;

	signIn(login, password)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.post('/autoLogin', verifyAdminToken, (req, res) => {
	res.status(200).send({ status: 200 });
});

router.post('/refreshTokens', verifyAdminRefreshToken);

module.exports = router;