const { verifyAdminToken } = require("../middlewares/jwtMiddleware");
const getFilms = require('../adminModels/adminFilms').getFilms;
const updateFilm = require('../adminModels/adminFilms').updateFilm;
const deleteFilm = require('../adminModels/adminFilms').deleteFilm;
const addFilm = require('../adminModels/adminFilms').addFilm;

const express = require('express'),
	router = express.Router();

router.get('/getFilms', verifyAdminToken, (req, res) => {
	getFilms()
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.post('/updateFilm', verifyAdminToken, (req, res) => {
	const { data } = req.body;

	updateFilm(data)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.delete('/deleteFilm', verifyAdminToken, (req, res) => {
	const { itemId } = req.body;

	deleteFilm(itemId)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.post('/addFilm', verifyAdminToken, (req, res) => {
	const { data } = req.body;

	addFilm(data, req.user.id)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

module.exports = router;