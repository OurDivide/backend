const db = require('../database/database'),
	SqlString = require('sqlstring');

const getFilms = () => {
	return new Promise((resolve, reject) => {
		const getFilmsSql = SqlString.format(`SELECT * from mediaContent`);

		db.all(getFilmsSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});
			if (rows && rows.length) resolve({
				status: 200,
				data: {
					films: rows
				}
			})
		});
	})
};

const updateFilm = newData => {
	const { id, title, filePath, previewPath, description, genres, releaseYear } = newData;
	return new Promise((resolve, reject) => {
		const updateFilmSql = SqlString.format(`
			UPDATE mediaContent
			SET title = ?, filePath = ?, previewPath = ?, description = ?, genres = ?, releaseYear = ?
			WHERE id = ?
		`, [title, filePath, previewPath, description, genres, releaseYear, id]);

		db.run(updateFilmSql, [], updateErr => {
			if (updateErr) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});

			const getUpdatedFilmSql = SqlString.format(`
				SELECT * FROM mediaContent WHERE id = ?
			`, [id]);

			db.get(getUpdatedFilmSql, [], (getErr, updatedFilm) => {
				if (getErr) reject({
					status: 500,
					data: {
						message: 'Something went wrong'
					}
				});

				if (updatedFilm) resolve({
					status: 200,
					data: {
						film: updatedFilm
					}
				})
			});
		});
	});
};

const deleteFilm = id => {
	return new Promise((resolve, reject) => {
		const deleteFilmSql = SqlString.format(`
			DELETE FROM mediaContent
			WHERE mediaContent.id = ?
		`, [id]);

		db.run(deleteFilmSql, [], (err) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});

			resolve({
				status: 200,
				message: `Film with id=${ id } was deleted`
			});
		});
	});
};

const addFilm = (data, id) => {
	return new Promise((resolve, reject) => {
		const addFilmSql = SqlString.format(`
			INSERT INTO mediaContent(id, title, filePath, previewPath, description, genres, releaseYear)
			VALUES (NULL, ?, ?, ?, ?, ?, ?)
		`, [data.title, data.filePath, data.previewPath, data.description, data.genres, data.releaseYear]);

		db.run(addFilmSql, [], addErr => {
			if (addErr) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});

			const getNewFilmSql = SqlString.format(`
			 SELECT * FROM mediaContent 
			 WHERE title=? AND filePath=? AND previewPath=? AND description=? AND genres=? AND releaseYear=?
			`, [data.title, data.filePath, data.previewPath, data.description, data.genres, data.releaseYear]);

			db.get(getNewFilmSql, [], (getErr, row) => {
				if (getErr) reject({
					status: 500,
					data: {
						message: `Something went wrong`
					}
				});

				if (row) {
					const addMarkSql = SqlString.format(`
						INSERT INTO userMarks(id, mediaId, userId, mark)
						VALUES(NULL, ?, ?, 5)
					`, [row.id, id]);

					db.run(addMarkSql, [], addMarkErr => {
						if (addMarkErr) reject({
							status: 500,
							data: {
								message: 'Something went wrong'
							}
						});

						resolve({
							status: 200,
							data: {
								film: row
							}
						});
					});


				}
			});
		})
	});
};

module.exports.getFilms = getFilms;
module.exports.updateFilm = updateFilm;
module.exports.deleteFilm = deleteFilm;
module.exports.addFilm = addFilm;