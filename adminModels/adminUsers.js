const db = require('../database/database');

const getUsers = () => {
	return new Promise((resolve, reject) => {
		const getUsersSql = `SELECT * FROM users`;

		db.all(getUsersSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});

			if (rows && rows.length) {
				resolve({
					status: 200,
					data: {
						users: rows
					}
				})
			}
		});
	});
};

module.exports.getUsers = getUsers;