const db = require('../database/database');

const getSubscriptions = () => {
	return new Promise((resolve, reject) => {
		const getSubscriptionsSql = `SELECT * FROM subscriptions`;

		db.all(getSubscriptionsSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});

			if (rows && rows.length) {
				resolve({
					status: 200,
					data: {
						subscriptions: rows
					}
				})
			}
		});
	});
};

module.exports.getSubscriptions = getSubscriptions;