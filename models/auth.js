const SqlString = require('sqlstring');
const bcrypt = require('bcrypt');
const generateTokens = require('../utils/generateToken');
const db = require('../database/database');

const signUp = (login, email, password) => {
	return new Promise((resolve, reject) => {
		checkUserExistence(login, password, email)
			.then(() => {
				resolve({
					status: 400,
					data: {
						message: 'User with this login or email already exist'
					}
				})
			})
			.catch(reason => {
				if (reason.status === 500) reject(reason);
				else {
					// Encrypt user password with salt = 10
					const encryptedPassword = bcrypt.hashSync(password, 10);

					// Sql for saving user to database
					const createUserSQL = SqlString.format(`
						INSERT INTO users(id, login, email, password, isAdmin)
						VALUES (null, ?, ?, ?, ?)
					`, [login, email, encryptedPassword, false]);

					// Run prepared SQL query
					db.run(createUserSQL, [], createErr => {
						// Error in database
						if (createErr) reject({
							status: 500,
							data: {
								message: 'Something went wrong'
							}
						});

						//SQL query for get this user
						const getUserId = SqlString.format(`
							SELECT id FROM users 
							WHERE login=? and email=?
						`, [login, email]);

						//Run SQL get query
						db.get(getUserId, [], (getIdErr, row) => {
							if (getIdErr) reject({
								status: 500,
								data: {
									message: 'Something went wrong'
								}
							});
							//Error if user wasn't found
							if (!row) reject({
								status: 400,
								data: {
									message: `User wasn't found`
								}
							});

							const id = { id: row.id };

							//Generates auth token
							const { token, refreshToken } = generateTokens(id);

							//Put token in to database
							const insertTokenSql = SqlString.format(`
								INSERT INTO tokens (id, userId, token, refreshToken)
 								VALUES (null, ?, ?, ?)
							`, [row.id, token, refreshToken]);

							//Run insert token query
							db.run(insertTokenSql, [], insertTokenErr => {
								if (insertTokenErr) reject(insertTokenErr);

								//if all step are successful, response tokens to user
								resolve({
									status: 200,
									data: {
										token,
										refreshToken
									}
								});
							})
						})
					});
				}
			});
	});
};

const checkUserExistence = (login, password, email) => {
	return new Promise((resolve, reject) => {
		const searchUserSql = SqlString.format(`SELECT id, password, isAdmin FROM users 
															 WHERE login=? or email=?`, [login, email ? email : login]);

		db.get(searchUserSql, [], (getIdErr, row) => {
			if (getIdErr) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
			if (!row) {
				reject({
					status: 400,
					data: {
						message: `User wasn't found`
					}
				});
			} else {
				if (bcrypt.compareSync(password, row.password)) {
					refreshTokens({ id: row.id }, row.isAdmin)
						.then(result => resolve(result))
						.catch(reason => reject(reason))
				} else reject({
					status: 400,
					data: {
						message: `Password is incorrect`
					}
				})
			}
		})
	});
};

const refreshTokens = (payload, isAdmin = 0) => {
	return new Promise((resolve, reject) => {
		const { token, refreshToken } = generateTokens(payload, isAdmin);

		const updateTokensSql = SqlString.format(`UPDATE tokens SET token=?, refreshToken=?
																WHERE userId=?`, [token, refreshToken, payload.id]);

		db.run(updateTokensSql, [], updateErr => {
			if (updateErr) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
			else resolve({
				status: 200,
				data: {
					token,
					refreshToken
				}
			})
		})
	});
};

module.exports.signUp = signUp;
module.exports.checkUserExistence = checkUserExistence;
module.exports.refreshTokens = refreshTokens;