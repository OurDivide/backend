const db = require('../database/database'),
	SqlString = require('sqlstring');

const getPayments = userId => {
	return new Promise((resolve, reject) => {
		const getUserPaymentsSql = SqlString.format(`
			SELECT t.duration, t.price, p.date FROM subscriptionTypes AS t, payments AS p
			WHERE p.userId = ? and p.typeId = t.id
		`, [userId]);

		db.all(getUserPaymentsSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
			if (!rows.length) {
				reject({
					status: 400,
					data: {
						message: `You haven't any payments`
					}
				})
			} else {
				resolve({
					status: 200,
					data: {
						payments: rows
					}
				});
			}
		});
	});
};

const getSubscriptionTypes = () => {
	return new Promise((resolve, reject) => {
		const getAllTypesSql = `SELECT * from subscriptionTypes`;

		db.all(getAllTypesSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
			if (rows.length) {
				resolve({
					status: 200,
					data: {
						types: rows
					}
				});
			}
		})
	})
};

const addToPayments = (userId, subscriptionType, date, expires) => {
	return new Promise((resolve, reject) => {
		const addToPaymentsSql = SqlString.format(`
			INSERT INTO payments(id, userId, typeId, date)
			VALUES (NULL, ?, ?, ?)
		`, [userId, subscriptionType, date]);

		db.run(addToPaymentsSql, [], addError => {
			if (addError) {
				reject({
					status: 500,
					data: {
						message: `Something went wrong`
					}
				})
			} else {
				resolve({
					status: 200,
					data: {
						expires
					}
				})
			}
		});
	})
};

const buySubscription = (userId, subscriptionType, isItFree = 0) => {
	return new Promise((resolve, reject) => {
		const getSubscriptionInfoSql = SqlString.format(`
			SELECT duration, price FROM subscriptionTypes WHERE id = ?
		`, [subscriptionType]);

		db.get(getSubscriptionInfoSql, [], (infoError, typeRow) => {
			if (infoError) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
			if (typeRow) {
				const getSubscriptionSql = SqlString.format(`
					SELECT expires, wasFreeUsed from subscriptions WHERE userId = ?
				`, [userId]);

				db.get(getSubscriptionSql, [], (subscriptionError, subscriptionRow) => {
					if (subscriptionError) reject({
						status: 500,
						data: {
							message: `Something went wrong`
						}
					});

					const paymentsDate = new Date().getTime();

					if (subscriptionRow) {
						let extendSubscriptionSql;

						if (subscriptionRow.wasFreeUsed && isItFree) reject({
							status: 400,
							data: {
								message: `You have already use free trial`
							}
						});

						const previousExpires = new Date(+subscriptionRow.expires),
							currentDate = new Date();

						let expires;

						if (previousExpires < currentDate) {
							expires = new Date(currentDate.setDate(currentDate.getDate() + typeRow.duration)).getTime();
						} else {
							expires = new Date(previousExpires.setDate(previousExpires.getDate() + typeRow.duration)).getTime();
						}

						extendSubscriptionSql = SqlString.format(`
							UPDATE subscriptions
							SET expires = ?, wasFreeUsed = ?
							WHERE userId = ?
						`, [expires, subscriptionRow.wasFreeUsed || isItFree, userId]);

						db.run(extendSubscriptionSql, [], extendError => {
							if (extendError) reject({
								status: 500,
								data: {
									message: 'Something went wrong'
								}
							});

							addToPayments(userId, subscriptionType, paymentsDate, expires)
								.then(result => {
									result.data.wasFreeUsed = subscriptionRow && subscriptionRow.wasFreeUsed ?
										subscriptionRow.wasFreeUsed : isItFree;

									resolve(result);
								})
								.catch(reason => reject(reason))
						})
					} else {
						let date = new Date(),
							expires = new Date(date.setDate(date.getDate() + typeRow.duration)).getTime();

						const makeSubscriberSql = SqlString.format(`
							INSERT INTO subscriptions(id, userId, wasFreeUsed, expires)
							VALUES (NULL, ?, ?, ?);
						`, [userId, isItFree, expires]);

						db.run(makeSubscriberSql, [], makeError => {
							if (makeError) {
								reject({
									status: 500,
									data: {
										message: `Something went wrong`
									}
								})
							} else {
								addToPayments(userId, subscriptionType, paymentsDate, expires)
									.then(result => {
										result.data.wasFreeUsed = subscriptionRow && subscriptionRow.wasFreeUsed ?
											subscriptionRow.wasFreeUsed : isItFree;

										resolve(result);
									})
									.catch(reason => reject(reason))
							}
						})
					}
				});
			} else reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
		})
	});
};

const getSubscriptionExpires = userId => {
	return new Promise((resolve, reject) => {
		const getExpiresSql = SqlString.format(`
			SELECT expires, wasFreeUsed FROM subscriptions
			WHERE userId = ?
		`, [userId]);

		db.get(getExpiresSql, [], (err, row) => {
			if (err) reject({
				status: 500,
				data: {
					message: `Something went wrong`
				}
			});
			if (row) {
				resolve({
					status: 200,
					data: {
						expires: row.expires,
						wasFreeUsed: row.wasFreeUsed
					}
				})
			} else {
				resolve({
					status: 204,
					data: {
						expires: 'false'
					}
				})
			}
		})
	});
};

module.exports.getPayments = getPayments;
module.exports.getSubscriptionTypes = getSubscriptionTypes;
module.exports.buySubscription = buySubscription;
module.exports.getSubscriptionExpires = getSubscriptionExpires;