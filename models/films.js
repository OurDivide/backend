const db = require('../database/database'),
	SqlString = require('sqlstring');

const getGenres = () => {
	return new Promise((resolve, reject) => {
		const getGenresSql = SqlString.format(`SELECT genre FROM genres`);

		db.all(getGenresSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});
			if (rows && rows.length) {
				resolve({
					status: 200,
					data: {
						genres: rows
					}
				})
			}
		})
	});
};

const getFilms = (firstUrlParam, currentPage) => {
	const filmsCountOnPage = 1;

	return new Promise((resolve, reject) => {
		const limitStartValue = (+currentPage - 1) * filmsCountOnPage;

		const getFilmsSql = SqlString.format(`
			SELECT m.id, m.title, m.previewPath, m.description, m.genres, m.releaseYear, AVG(u.mark) as mark
			FROM mediaContent as m, userMarks as u
			WHERE m.id = u.mediaId AND (m.genres LIKE ? OR m.title LIKE ?)
			GROUP BY u.mediaId
			ORDER BY mark DESC
			LIMIT ?, ?
		`, [`%${ firstUrlParam }%`, `%${ firstUrlParam }%`, limitStartValue, filmsCountOnPage]);

		db.all(getFilmsSql, [], (err, rows) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});
			if (rows && rows.length) {
				const getFilmsCountSql = SqlString.format(`
					SELECT COUNT(*) as filmsCount FROM mediaContent WHERE genres LIKE ? OR title LIKE ?
				`, [`%${ firstUrlParam }%`, `%${ firstUrlParam }%`]);

				db.get(getFilmsCountSql, [], (getCountErr, { filmsCount }) => {
					if (getCountErr) reject({
						status: 500,
						data: {
							message: 'Something went wrong'
						}
					});
					if (filmsCount) {
						resolve({
							status: 200,
							data: {
								films: rows,
								pagesCount: Math.ceil(filmsCount / filmsCountOnPage)
							}
						});
					}
				});
			} else resolve({
				status: 200,
				data: {
					films: []
				}
			});
		});
	});
};

const getFilmPath = filmId => {
	return new Promise(((resolve, reject) => {
		const getFilePathSql = SqlString.format(`
			SELECT filePath FROM mediaContent WHERE id = ?
			`, [ +filmId ]);

		db.get(getFilePathSql, [], (err, row) => {
			if (err) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});
			if (row) resolve({
				filePath: row.filePath
			})
		});
	}))
};

const rateFilm = (userId, filmId, mark) => {
	return new Promise((resolve, reject) => {
		const checkUserMarkSql = SqlString.format(`
			SELECT * FROM userMarks WHERE userId = ? AND mediaId = ?
		`, [userId, filmId]);

		db.get(checkUserMarkSql, [], (checkErr, row) => {
			if (checkErr) reject({
				status: 500,
				data: {
					message: 'Something went wrong'
				}
			});
			if (row) {
				resolve({
					status: 208,
					data: {
						message: 'You have already marked this video'
					}
				})
			} else {
				const addNewMarkSql = SqlString.format(`
					INSERT INTO userMarks(id, mediaId, userId, mark) 
					VALUES(NULL, ?, ?, ?)
				`, [filmId, userId, mark]);

				db.run(addNewMarkSql, [], addErr => {
					if (addErr) {
						reject({
							status: 500,
							data: {
								message: 'Something went wrong'
							}
						});
					} else {
						const getNewUserMarksSql = SqlString.format(`
							SELECT m.id, AVG(u.mark) as mark
							FROM mediaContent as m, userMarks as u
							WHERE m.id = u.mediaId AND m.id = ? AND u.mark > 0
						`, [filmId]);

						db.get(getNewUserMarksSql, [], (getErr, markObj) => {
							if (getErr) reject({
								status: 500,
								data: {
									message: 'Something went wrong'
								}
							});
							if (markObj) resolve({
								status: 200,
								data: {
									mark: markObj.mark
								}
							});
						});
					}
				})
			}
		});
	});
};

const getUserMarks = (userId) => {
	return new Promise((resolve, reject) => {
			const getUserMarkSql = SqlString.format(`
				SELECT mediaId, mark FROM userMarks WHERE userId = ?
			`, [userId]);

			db.all(getUserMarkSql, [], (err, rows) => {
				if (err) reject({
					status: 500,
					data: {
						message: 'Something went wrong'
					}
				});
				if (rows) {
					resolve({
						status: 200,
						data: {
							marks: rows
						}
					})
				} else {
					resolve({
						status: 204,
						data: {
							message: 'You`re haven`t any marks'
						}
					})
				}
			});
	});
};



module.exports.getGenres = getGenres;
module.exports.getFilms = getFilms;
module.exports.getFilmPath = getFilmPath;
module.exports.rateFilm = rateFilm;
module.exports.getUserMarks = getUserMarks;