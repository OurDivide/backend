const signInMiddleware = (req, res, next) => {
	const { login, password } = req.body;

	let message = 'Your',
		counter = 0;

	if (login.length < 6) {
		message += ' login';
		counter++;
	}
	if (password.length < 8) {
		message += counter ? ' and password' : ' password';
		counter++;
	}

	message += counter === 1 ? ' is too short' : ' are too short';

	if (counter) res.status(401).send(message);

	next();
};

const signUpMiddleware = (req, res, next) => {
	const { login, email, password } = req.body;

	let message = 'Your',
		counter = 0;

	if (login.length < 6) {
		message += ' login';
		counter++;
	}
	if (email.length < 6) {
		message += counter ? ', email' : ' email';
		counter++;
	}
	if (password.length < 8) {
		message += counter ? ', password' : ' password';
		counter++;
	}

	message += counter === 1 ? ' is too short' : ' are too short';

	if (counter) res.status(401).send(message);
	else next();
};

module.exports.signInMiddleware = signInMiddleware;
module.exports.signUpMiddleware = signUpMiddleware;