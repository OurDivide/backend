const jwt = require('jsonwebtoken');
const { secret, refreshSecret, adminSecret, adminRefreshSecret } = require("../configs/tokenConfig");
const { refreshTokens } = require('../models/auth');

const verifyToken = (req, res, next) => {
	//Get authorization header from request
	const authHeader = req.headers.authorization;

	//If header was passed
	if (authHeader) {
		//Get token from header
		const token = authHeader.split(' ')[1];

		//verify token
		jwt.verify(token, secret, (err, decoded) => {
			//if token expired server send response with this information
			if (err && err.name === 'TokenExpiredError') {
				res.status(401).send({
					status: 401,
					data: {
						message: 'Token was expired'
					}
				});
			}

			//If token isn't valid server send response with this information
			if (err && err.name === 'JsonWebTokenError') return res.status(401).send({
				status: 401,
				data: {
					message: `Token isn't valid`
				}
			});

			//If token is valid add user id to request and go to the original request
			if (decoded) {
				req.user = { id: decoded.id };
				next();
			}
		});

	//If token wasn't passed with request headers
	} else res.status(403).send({
		status: 403,
		data: {
			message: 'Please sign in'
		}
	});
};

const verifyRefreshToken = (req, res) => {
	//Get refreshToken from request body
	const { refreshToken } = req.body;

	//Verify token
	jwt.verify(refreshToken, refreshSecret, (err, decoded) => {
		//If verify cause an error, server send response with this error
		if (err) return res.status(403).send({
			status: 403,
			data: {
				message: 'Please login again'
			}
		});

		//Get new pair of tokens and send them to client
		refreshTokens({ id: decoded.id })
			.then(result => res.status(200).send({
				status: 200,
				data: {
					...result
				}
			}))
			.catch(reason => res.status(reason.status).send(reason))
	});
};

const verifyAdminToken = (req, res, next) => {
	const authHeader = req.headers.authorization;

	if (authHeader) {
		const token = authHeader.split(' ')[1];

		jwt.verify(token, adminSecret, (err, decoded) => {
			if (err && err.name === 'TokenExpiredError') {
				res.status(401).send({
					status: 401,
					data: {
						message: 'Token was expired'
					}
				});
			}

			if (err && err.name === 'JsonWebTokenError') return res.status(401).send({
				status: 401,
				data: {
					message: `Token isn't valid`
				}
			});

			if (decoded) {
				req.user = { id: decoded.id };
				next();
			}
		});
	} else res.status(403).send({
		status: 403,
		data: {
			message: 'Please sign in'
		}
	});
};

const verifyAdminRefreshToken = (req, res) => {
	const { refreshToken } = req.body;

	jwt.verify(refreshToken, adminRefreshSecret, (err, decoded) => {
		if (err) return res.status(403).send({
			status: 403,
			data: {
				message: 'Please login again'
			}
		});

		refreshTokens({ id: decoded.id }, 1)
			.then(result => res.status(200).send({
				status: 200,
				data: {
					...result
				}
			}))
			.catch(reason => res.status(reason.status).send(reason))
	});
};

module.exports.verifyToken = verifyToken;
module.exports.verifyRefreshToken = verifyRefreshToken;
module.exports.verifyAdminToken = verifyAdminToken;
module.exports.verifyAdminRefreshToken = verifyAdminRefreshToken;