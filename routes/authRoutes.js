const { verifyToken, verifyRefreshToken } = require("../middlewares/jwtMiddleware");
const { signInMiddleware, signUpMiddleware } = require('../middlewares/authMiddleware');
const signUp = require('../models/auth').signUp;
const signIn = require('../models/auth').checkUserExistence;

const express = require('express'),
	router = express.Router();

router.post('/signIn', signInMiddleware, (req, res) => {
	const { login, password } = req.body;

	signIn(login, password)
		.then(result => {
			res.status(result.status).send(result)
		})
		.catch(reason => {
			res.status(reason.status).send(reason)
		})
});

router.post('/signUp', signUpMiddleware, (req, res) => {
	const { login, email, password } = req.body;

	signUp(login, email, password)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.post('/autoLogin', verifyToken, (req, res) => {
	res.status(200).send({ status: 200 });
});

router.post('/refreshTokens', verifyRefreshToken);

module.exports = router;