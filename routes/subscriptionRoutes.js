const { verifyToken } = require("../middlewares/jwtMiddleware");
const getPayments = require('../models/subscriptions').getPayments;
const getSubscriptionTypes = require('../models/subscriptions').getSubscriptionTypes;
const buySubscription = require('../models/subscriptions').buySubscription;
const getSubscriptionExpires = require('../models/subscriptions').getSubscriptionExpires;

const express = require('express'),
	router = express.Router();

router.post('/getPayments', verifyToken, (req, res) => {
	getPayments(req.user.id)
		.then(result => res.status(result.status).send(result))
		.catch(reason => {
			res.status(reason.status).send(reason)
		})
});

router.get('/getSubscriptionTypes', (req, res) => {
	getSubscriptionTypes()
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.post('/buySubscription', verifyToken, (req, res) => {
	const { typeId, isItFree } = req.body;

	buySubscription(req.user.id, typeId, isItFree)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.get('/getSubscriptionExpires', verifyToken, (req, res) => {
	getSubscriptionExpires(req.user.id)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

module.exports = router;