const { verifyToken } = require("../middlewares/jwtMiddleware");
const path = require('path');
const fs = require('fs');
const getGenres = require('../models/films').getGenres;
const getFilms = require('../models/films').getFilms;
const getFilmPath = require('../models/films').getFilmPath;
const rateFilm = require('../models/films').rateFilm;
const getUserMarks = require('../models/films').getUserMarks;
const getSubscription = require('../models/subscriptions').getSubscriptionExpires;

const express = require('express'),
	router = express.Router();

router.get('/getGenres', (req, res) => {
	getGenres()
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.get('/getFilms/:genre/:page', (req, res) => {
	const { genre, page } = req.params;

	getFilms(genre, page)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.post('/rateFilm', verifyToken, (req, res) => {
	const { user: { id } } = req;
	const { filmId, mark } = req.body;

	rateFilm(id, filmId, mark)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.get('/getMarks', verifyToken, (req, res) => {
	getUserMarks(req.user.id)
		.then(result => res.status(result.status).send(result))
		.catch(reason => res.status(reason.status).send(reason))
});

router.get('/watch/:id', (req, res) => {
	const { id } = req.params;

	const range = req.headers.range;

	if (!range) {
		res.status(400).send({
			status: 400,
			data: {
				message: 'Require Range Headers'
			}
		});
	}

	getFilmPath(id)
		.then(getPathResult => {
			// get video stats (about 61MB)
			const videoPath = path.resolve(`${ rootPath }/${ getPathResult.filePath }`);
			const videoSize = fs.statSync(videoPath).size;

			// Parse Range
			// Example: "bytes=32324-"
			const CHUNK_SIZE = 10 ** 6; // 1MB
			const start = Number(range.replace(/\D/g, ""));
			const end = Math.min(start + CHUNK_SIZE, videoSize - 1);

			// Create headers
			const contentLength = end - start + 1;
			const headers = {
				"Content-Range": `bytes ${ start }-${ end }/${ videoSize }`,
				"Accept-Ranges": "bytes",
				"Content-Length": contentLength,
				"Content-Type": "video/mp4",
			};

			// HTTP Status 206 for Partial Content
			res.writeHead(206, headers);

			// create video read stream for this particular chunk
			const videoStream = fs.createReadStream(videoPath, { start, end });

			// Stream the video chunk to the client
			videoStream.pipe(res);
		})
		.catch(e => console.log(e));
});

module.exports = router;