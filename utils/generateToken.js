const jwt = require('jsonwebtoken');
const { secret, refreshSecret, tokenLifeTime,
	refreshTokenLifeTime, adminSecret, adminRefreshSecret } = require('../configs/tokenConfig');

const generateTokens = (payload, isAdmin, tokenExpires, refreshTokenExpires) => {
	const secretJWT = +isAdmin ? adminSecret : secret;
	const refreshSecretJWT = +isAdmin ? adminRefreshSecret : refreshSecret;

	return {
		token: jwt.sign(payload, secretJWT, { expiresIn: tokenExpires || tokenLifeTime }),
		refreshToken: jwt.sign(payload, refreshSecretJWT, { expiresIn: refreshTokenExpires || refreshTokenLifeTime })
	}
};

module.exports = generateTokens;