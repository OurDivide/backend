const express = require('express'),
	bodyParser = require('body-parser'),
	cors = require('cors'),
	path = require('path'),
	db = require('./database/database'),
	authRouter = require('./routes/authRoutes'),
	subscriptionRouter = require('./routes/subscriptionRoutes'),
	filmRouter = require('./routes/filmRoutes'),
	adminAuthRouter = require('./adminRoutes/authRoutes'),
	adminFilmRouter = require('./adminRoutes/filmRoutes'),
	adminSubscriptionRouter = require('./adminRoutes/subscriptionRoutes'),
	adminUserRouter = require('./adminRoutes/userRoutes');

global.rootPath = path.dirname(require.main.filename);

let app = express();

const corsOptions = {
	origin: ['http://localhost:3000', 'http://localhost:8080'],
	optionsSuccessStatus: 200
};

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors(corsOptions));

app.use('/films', express.static(__dirname + '/films'));

app.use('/api', authRouter);
app.use('/api', subscriptionRouter);
app.use('/api', filmRouter);

app.use('/admin', adminAuthRouter);
app.use('/admin', adminFilmRouter);
app.use('/admin', adminSubscriptionRouter);
app.use('/admin', adminUserRouter);

app.listen('3001', () => {
	console.log(`Example app listening at http://localhost:3001`)
});